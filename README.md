# The-HACK-General-Purpose-Computer

## What we’ll achieve:

In this project-centered course* we will build a modern computer system, from the ground up. We’ll divide this fascinating journey into six hands-on projects that will take we from constructing elementary logic gates all the way through creating a fully functioning general purpose computer. In the process, we will learn - in the most direct and constructive way - how computers work, and how they are designed.

 we will need no physical materials, since we will build the computer on our own PC, using a software-based hardware simulator, just like real computers are designed by computer engineers in the field.

## References:
* **Course Part1** : [Build a Modern Computer from First Principles: From Nand to Tetris (Project-Centered Course)](https://www.coursera.org/learn/build-a-computer)

* **Course Part2** : [Build a Modern Computer from First Principles: Nand to Tetris Part II (project-centered course)](https://www.coursera.org/learn/nand2tetris2)

## General Requirements:
* **Course General Files** : [Drive Folder for all softwares](https://drive.google.com/file/d/1xZzcMIUETv3u3sdpM_oTJSTetpVee3KZ/view)

* **Java** : [java language to run the softwares](https://www.java.com/download/ie_manual.jsp)

* **The Course Founders Website** : [for more details, go here](https://www.nand2tetris.org/software)

## What we will cover?

* **Hardware:** Logic gates, Boolean arithmetic, multiplexors, flip-flops, registers,
RAM units, counters, Hardware Description Language (HDL), chip simulation and
testing.
* **Architecture:** ALU/CPU design and implementation, machine code, assembly
language programming, addressing modes, memory-mapped input/output (I/O).
* **Operating systems:** Memory management, math library, basic I/O drivers,
screen management, file I/O, high-level language support.
* **Programming languages:** Object-based design and programming, abstract data
types, scoping rules, syntax and semantics, references.
* **Compilers:** Lexical analysis, top-down parsing, symbol tables, virtual stackbased
machine, code generation, implementation of arrays and objects.
* **Data structures and algorithms:** Stacks, hash tables, lists, recursion, arithmetic
algorithms, geometric algorithms, running time considerations.
* **Software engineering:** Modular design, the interface/implementation paradigm,
API design and documentation, proactive test planning, programming at the large,
quality assurance.

## What is going on here in this series of projects?
* **Project 1** : Elementary Logic Gates